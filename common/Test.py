# coding=utf-8
# encoding: utf-8 -*-
# @time   : 2022/11/20 16:47
# @author : zyx
# @file   : Test.py
import time, hashlib

# 获取当前时间
lt = time.strftime('%m%d%H%M%S', time.localtime())

import random
import string
import hmac
import base64
from hashlib import sha256
from base64 import b64encode
t =time.time()
print(str(round(t*1000)))


cspid = "test0531144558"
cspToken ="1273cc0531144558"
datasha = cspToken + str(round(t*1000)) # 要进行加密的数据
data_sha = hashlib.sha256(datasha.encode('utf-8')).hexdigest()
base64data = cspid+ ":" +data_sha
base64_data = b64encode(bytearray(base64data.encode()))
token1 = "Basic "+ str(base64_data,'utf-8')
print(token1)
